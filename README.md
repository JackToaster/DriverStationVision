# Driver station vision app  
This is a program to determine which direction the scale is tipping and send the data to the robot.

Run the main.py program with your desired camera, robot IP, and port (Defaults to team 3641's robot IP and port 5801). Command line options to do this can be found with ./main.py -h

The program sends comma separated values over UDP with this format: current angle, predicted angle in 1 second, predicted in 2 seconds, predicted in 3 seconds. Ex: 02.230,03.450,06.780,-12.00
