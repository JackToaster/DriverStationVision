#!env/bin/python
import socket
import numpy as np
import cv2
import argparse
import sys

WIN_NAME = 'image'
#sliding window size for rate/angle filtering
ANGLE_WINDOW_SIZE = 40
#Ignore angle measurements greater than this
SCALE_MAX_ANGLE = 20
#Don't return angles greater than this
SCALE_ENDSTOP = 12
#How much to underestimate movement
PREDICT_RATE_MULT = 0.5


# read command line arguments
parser = argparse.ArgumentParser(description='Detect the position of the scale and send it to the robot.')
parser.add_argument('--res', default=[640,480], type=int, nargs=2, help='Sets the resolution (x,y)')
parser.add_argument('--camera', default=0, help='Sets the ID of the camera to use or name of the video file to read from')

parser.add_argument('--cameran', default=-1, type=int, help='Sets the ID of the camera to use or name of the video file to read from')
parser.add_argument('--ip', default='roboRIO-3641-FRC.local', help='Set the ip of the robot to send UDP packets to')
parser.add_argument('--port', default=5801, type=int, help='Set the ip of the robot to send UDP packets to')
parser.add_argument('--exp', default=-5, type=float, help='Set the camera exposure')

#parser.add_argument('--loop', default=0, help='Whether to loop the video input')
args = parser.parse_args()

res = args.res

liveMode = True

lower_hsv = [0,0,0]
upper_hsv = [0,0,0]

aoi_box = [[0,0],[res[0],res[1]]]


# Open the video capture device
if args.cameran != -1:
    cap = cv2.VideoCapture(args.cameran)
else:
    cap = cv2.VideoCapture(args.camera)
cap.set(15, args.exp)

#Create window
cv2.namedWindow(WIN_NAME)
#cv2.setMouseCallback(WIN_NAME, on_mouse)

# required for trackbar functions that do nothing
def nothing(x):
    pass

# make it easier to read a trackbar in the window
def slider(name):
    return cv2.getTrackbarPos(name, WIN_NAME)

#Create trackbars for image correction
cv2.createTrackbar('H min',WIN_NAME,0,255,nothing)
cv2.createTrackbar('S min',WIN_NAME,0,255,nothing)
cv2.createTrackbar('V min',WIN_NAME,0,255,nothing)

cv2.createTrackbar('H max',WIN_NAME,0,255,nothing)
cv2.createTrackbar('S max',WIN_NAME,0,255,nothing)
cv2.createTrackbar('V max',WIN_NAME,0,255,nothing)

#stores the values from the last angle readings
angles = []


# Reads the angle readings and returns the average.
def getAngle():
    if len(angles) > 0:
        return np.mean(angles)
    else:
        return 0



# Predicts the future angle based on the current rate of change.
def predictAngle(time):
    #Make sure there's enough angles for prediction
    if len(angles) > 1:
        # Split the samples in two
        half_i = int(len(angles) / 2)
	fr = float(cap.get(cv2.CAP_PROP_FPS))
	if fr == 0:
		fr = 30
        half_i_sec = float(half_i) / fr
        # Determine the rate based on the two subsets of samples
        rate = (np.mean(angles[half_i:]) - np.mean(angles[:half_i])) / half_i_sec
        # Multiply some stuff ang get the predicted position
        # Uses PREDICT_RATE_MULT to kinda model the friction of the scale.
        out = getAngle() + rate * time * PREDICT_RATE_MULT
        # Limit output to scale's actual angle limits
        if out > SCALE_ENDSTOP:
            return SCALE_ENDSTOP
        elif out < -SCALE_ENDSTOP:
            return -SCALE_ENDSTOP
        else:
            return out
    # If only 1 sample is available return that.
    elif len(angles) > 0:
        return angles[0]
    # Assume it's centered before reading any angles.
    else:
        return 0


# setup UDP communication
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
UDP_IP = args.ip
UDP_PORT = args.port

# Send a UDP message to the robot
def udp_send(message):
    sock.sendto(message, (UDP_IP, UDP_PORT))

def drawRect(rect1):
    box1 = cv2.boxPoints(rect1)
    boxes = np.int0(box1)
    cv2.drawContours(cropped, [boxes],0,(0,200,0),2)

def getMask(img):
    # Gaussian blur for better blob detection
    blur = cv2.GaussianBlur(img,(5,5),0)

    # Convert to Hue, Saturation, and Value colorspace
    hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)

    # Threshold and erode the threshold
    # (Erode removes tiny flecks by doing a kernel operation on black pixels)
    thresh = cv2.inRange(hsv, lower_hsv, upper_hsv)
    thresh = cv2.erode(thresh, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3)), 1)
    return thresh


# Repeat until 'q' is pressed to quit
while(True):
    #print aoi_box
    # Capture frame-by-frame
    ret, frame = cap.read()
    
    # Resize frame
    # Try-Except is to restart video loop.
    try:
        frame = cv2.resize(frame, (res[0],res[1]))
    except cv2.error:
        cap = cv2.VideoCapture(args.camera)
        ret, frame = cap.read()
        frame = cv2.resize(frame, (res[0],res[1]))
    # Define the color range based on sliders and show the live window
    # if live mode is enabled
    if liveMode:
        lower_hsv = np.array(map(slider, ['H min', 'S min', 'V min']))
        upper_hsv = np.array(map(slider, ['H max', 'S max', 'V max']))
        
    # Perform contour detection
    cropped = frame[aoi_box[0][1]:aoi_box[1][1],aoi_box[0][0]:aoi_box[1][0]]
    thresh = getMask(cropped)
    im2,contours,hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        
    if liveMode:
        # Mask for visualization during calibration
        masked = cv2.bitwise_and(cropped, cropped, mask=thresh)
        
        cv2.imshow('image', masked)


    # Don't act on any contours found unless they're found!
    if len(contours) >= 1:
        #find the biggest area
        contours = sorted(contours, key=cv2.contourArea, reverse = True)
        c1 = contours[0]

        #Find the rectangle of a scale arm
        rect1 = cv2.minAreaRect(c1)
        
        #draw the rectangle
        drawRect(rect1)
        
        #Read the angle (-90 to 0 for some reason)
        rawAngle = rect1[2]
        
        #convert it to a usable angle
        angle = rawAngle
        if rawAngle < -45:
           angle = rawAngle + 90
        
        #Ignore angles outside a usable range
        if abs(angle) < SCALE_MAX_ANGLE:
            angles.append(angle)
            if len(angles) > ANGLE_WINDOW_SIZE:
                angles.pop(0)
            
            

    cv2.imshow('tracking', cropped)
    
    # Create a message to send over UDP
    # Consists of current, angle in 1 sec, in 2 sec, and in 3 sec.
    comma_sep_data = '%06.3f,%06.3f,%06.3f,%06.3f' % (getAngle(), predictAngle(1), predictAngle(2), predictAngle(3))
    # Print data for debugging
    print(comma_sep_data)

    # Actually send the data    
    udp_send(comma_sep_data)
    
    # Check if the q key is pressed to exit the application
    key = cv2.waitKey(1) & 0xFF
    if key == ord('q'):
        break
    elif key == ord('s'):
        liveMode = False
        cv2.destroyWindow('image')
    elif key == ord('b'):
        roi = cv2.selectROI(frame)
        aoi_box = [[roi[0],roi[1]],[roi[0] + roi[2],roi[1] + roi[3]]]
        cv2.destroyWindow('ROI selector')



# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

